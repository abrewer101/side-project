from django.urls import path
from word_of_the_day.views import show_recipe

urlpatterns = [
    path("word_of_the_day/",  show_recipe),
]
