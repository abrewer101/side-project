from django.apps import AppConfig


class WordOfTheDayConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'word_of_the_day'
